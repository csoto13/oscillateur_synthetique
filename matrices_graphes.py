import numpy as np

# graphes dans l'ordre lexicographique sous forme de matrice sans seuils (-1 : inhibition, 0 : aucune influence)
graphe_14 = np.array([[0,-1,0,0],[0,0,-1,-1],[-1,0,0,0],[0,-1,0,0]])
graphe_20 = np.array([[0,-1,0,-1],[0,0,-1,-1],[-1,0,0,0],[0,-1,0,0]])
graphe_9 = np.array([[0,-1,0,-1],[-1,0,-1,0],[-1,0,0,0],[0,-1,0,0]])
graphe_1 = np.array([[0,-1,-1,0],[-1,0,-1,0],[-1,-1,0,0],[0,0,0,0]])
graphe_11 = np.array([[0,-1,0,-1],[-1,0,-1,0],[-1,0,0,-1],[0,0,-1,0]])
graphe_12 = np.array([[0,-1,0,-1],[-1,0,-1,0],[-1,0,0,-1],[0,-1,0,0]])
graphe_3 = np.array([[0,-1,-1,0],[-1,0,-1,0],[-1,0,0,-1],[0,-1,0,0]])
graphe_13 = np.array([[0,-1,0,-1],[-1,0,-1,0],[-1,0,0,-1],[0,-1,-1,0]])

# liste du nom des graphes
list_graphes = ['graphe_14', 'graphe_20', 'graphe_9', 'graphe_1', 'graphe_11', 'graphe_12', 'graphe_3', 'graphe_13']

# liste des graphes
list_mat_graphes = [graphe_14, graphe_20, graphe_9, graphe_1, graphe_11, graphe_12, graphe_3, graphe_13]
