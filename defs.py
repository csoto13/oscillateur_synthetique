import numpy as np
import itertools


def deg_sortant(graph):
    """ Renvoie le degré sortant de chaque variable sous forme de tableau
        graph : matrice carrée correspondant à un graphe d'influence """
    return np.sum(abs(graph), 1)


def deg_entrant(graph):
    """ Renvoie le degré entrant de chaque variable sous forme de tableau
        graph : matrice carrée correspondant à un graphe d'influence """
    return np.sum(abs(graph), 0)


def combinaison(graph):
    """ Renvoie la liste de toutes les combinaisons possibles des composantes des matrices où il y a une inhibition
       graph : matrice carrée correspondant à un graphe d'influence """

    # initiation de val, qui représente les valeurs que peut prendre une variable qui a deux seuils
    val = np.empty((0, 2))
    for i in deg_sortant(graph):
        # si d+(v)=2, alors v peut avoir deux seuils : -1 ou -2 (on ne considère que les inhibitions)
        if i == 2:
            # on met deux fois le vecteur (-1, -2) car v a deux inhibitions
            val = np.vstack([val, [-1, -2], [-1, -2]])

    # Créer toutes les combinaisons possibles de val
    combi = list(itertools.product(*val))

    # Récupère les indices où les seuils d'une même varibale sont égaux et les supprime
    index = []
    for i in range(len(combi)):
        for j in range(int(len(combi[i]) / 2)):
            if (combi[i][2 * j] == combi[i][2 * j + 1]):
                index.append(i)
                break

    for i in sorted(index, reverse=True):
        del combi[i]

    return combi


def list_graphes_combi(graph):
    """ Renvoie la liste de tous les graphes d'influence d'un même graphe de base non étiqueté
       Attention : les graphes ne sont pas séparées, il s'agit ici de la liste des lignes des matrices de tous les graphes d'influence
       graph : matrice carrée correspondant à un graphe d'influence """
    n_graph = len(graph)
    d_plus = deg_sortant(graph)
    ind_deg_sort_2 = np.where(d_plus == 2)
    ind_diff_0 = np.where(graph != 0)
    mat = graph.copy()
    list_combi = combinaison(graph)

    j = 0
    # boucle sur le nombre de combinaisons possibles
    for k in range(len(list_combi)):
        # boucle sur les lignes dont le degré sortant = 2
        for i in range(len(ind_diff_0[0])):
            if ind_diff_0[0][i] in ind_deg_sort_2[0]:
                # modifie les valeurs de mat pour mettre celle de la liste des combinaisons
                mat[ind_diff_0[0][i]][ind_diff_0[1][i]] = list_combi[k][j]
                j += 1
        # ajoute la nouvelle combinaison de matrice au graphe de base
        graph = np.vstack((graph, mat))
        j = 0
    return graph


def PARA_deg_2(f, v, reg_var, nom_var, pred_boucle, sv):
    """ Ajoute les paramètres respectant les états caractéristiques et les arcs fonctionnels de la boucle souhaitée dans le fichier f
       f           : fichier
       v           : indice sur les variables
       reg_var     : régulation d'une seule variable
       nom_var     : nom des variables d'un graphe d'influence
       pred_boucle : les variables précédentes dans la boucle souhaitée
       sv          : le seuil de la variable v sur son successeur dans la boucle """

    # récupère la régulation précédente dans la boucle souhaitée
    reg_boucle = list(filter(lambda x: '%s_To' % pred_boucle[nom_var[v]] in x, reg_var))[0]
    # récupère l'autre régulation qui n'est pas dans la boucle
    non_reg_boucle = list(filter(lambda x: '%s_To' % pred_boucle[nom_var[v]] not in x, reg_var))[0]

    # les valeurs des paramètres diffèrent en fonction du seuil
    if sv == 1:
        val = [1, 0]
    if sv == 2:
        val = [2, 1]

    # écriture des 4 paramètres dans le fichier f
    f.write('K_%s = 0 ;\n\n' % nom_var[v])
    f.write('K_%s:%s = %s ;\n\n' % (nom_var[v], reg_boucle, val[0]))
    f.write('K_%s:%s = %s ;\n\n' % (nom_var[v], non_reg_boucle, val[1]))
    f.write('K_%s:%s:%s = 2 ;\n\n' % (nom_var[v], reg_boucle, non_reg_boucle))

