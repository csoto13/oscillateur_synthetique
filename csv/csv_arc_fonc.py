import pandas
import numpy as np
import os


def arc_fonc(file_name, ext):
    """ Fonction qui créer un nouveau fichier rectifiant les paramétrisations OK en KO si un arc sur D n'est pas fonctionnel selon la définition
    file_name : nom du fichier (sans l'extension)
    ext       : extension du fichier """

    # créer une dataframe en récupérant un fichier .csv qui est la sortie de TotemBioNet
    df = pandas.read_csv(file_name + ext, delimiter=';', header=None, error_bad_lines=False)

    # récupère le nombre de modèle testé (on enlève la première ligne qui est le nom des colonnes)
    nb_mod_test = df.shape[0] - 1

    # supprime la dernière colonne (error explanation)
    del df[df.columns[-1]]

    # supprime les lignes où les paramétrisations sont KO
    index_ko = np.where(df[df.columns[1]] == 'KO')[0]
    df.drop(index=index_ko, inplace=True)

    # s'il s'agit d'un graphe ou D est absent on n'a pas besoin de traiter les arcs fonctionnels
    if df.iloc[0].str.contains('K_D').any():

        # récupère le degré entrant de D
        nb_para_D = 0
        deg_ent_D = 0

        # boucle sur les colonnes de la ligne 0 (liste des paramètres) et regarde combien D a de paramètres
        for i in df.iloc[0]:
            if "K_D" in i:
                nb_para_D = nb_para_D + 1

        # si D a 4 paramètres, alors son degré entrant est égal à 2
        if (nb_para_D == 4):
            deg_ent_D = 2
        # si D a 2 paramètres, alors son degré entrant est égal à 1
        elif (nb_para_D == 2):
            deg_ent_D = 1
        else:
            print('Il y a une erreur sur les paramètres de D')

        # récupère les indices des lignes où le ou les arcs sur D n'est (ne sont) pas fonctionnel(s)
        # si d-(v)=1, alors on regarde si K_D = K_D,x (x n'a pas d'influence D)
        if deg_ent_D == 1:
            index_ = np.where(df[df.columns[-2]] == df[df.columns[-1]])[0]
        # si d-(v)=2, alors on regarde si (K_D = K_D,x et K_D,y = K_D,xy ; x n'a pas d'influence sur D) ou si (K_D = K_D,y et K_D,x = K_D,xy ; y n'a pas d'influence sur D)
        elif deg_ent_D == 2:
            index_x = np.where(
                np.logical_and(df[df.columns[-4]] == df[df.columns[-3]], df[df.columns[-2]] == df[df.columns[-1]]))[0]
            index_y = np.where(
                np.logical_and(df[df.columns[-4]] == df[df.columns[-2]], df[df.columns[-3]] == df[df.columns[-1]]))[0]
            index_ = list(set().union(index_x, index_y))
        else:
            print(file_name)
            print('deg ent D:', deg_ent_D)
            print('Il y a une erreur sur le degré entrant de D')

        # supprime toutes les paramétrisations KO
        df.drop(index=index_, inplace=True)

    # récupèle le nombre de modèle OK
    nb_mod_OK = df.shape[0] - 1

    # créer un nouveau fichier : _af pour Arc Fonctionnel
    df.to_csv('csv_af/%s_af%s' % (file_name, ext), sep=';', index=False)

    return nb_mod_test, nb_mod_OK


fileDir = r"C:\Users\coral\Documents\POLYTECH\STAGE_MAM5\projet_stage_mam5\csv"
fileExt = r".csv"
list_file = [_ for _ in os.listdir(fileDir) if _.endswith(fileExt)]

nb_mod_test = 0
nb_mod_OK = 0
# boucle sur les fichiers .csv
for file in list_file:
    file_name = os.path.splitext(file)[0]
    ext = os.path.splitext(file)[1]
    arc_fonc_ = arc_fonc(file_name, ext)
    nb_mod_test += arc_fonc_[0]
    nb_mod_OK += arc_fonc_[1]

print('TotemBioNet a testé %s modèles.' % nb_mod_test)
print('Au total, nous avons trouvés %s réseaux de régulation.' % nb_mod_OK)




