# créer les sorties .out et .csv de tous les fichiers .smb
list_smb=`ls *.smb`
for i in $list_smb
do
	totembionet $i -csv
done

# déplacer les fichiers .out dans le dossier .out
list_out=`ls *.out`
for i in $list_out
do
	mv $i ~/Documents/programmes/totembionet-master/oscillateur/out
done

# déplacer les fichiers .csv dans le dossier .csv
list_csv=`ls *.csv`
for i in $list_csv
do
	mv $i ~/Documents/programmes/totembionet-master/oscillateur/csv
done
