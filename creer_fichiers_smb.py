import numpy as np
import defs
from matrices_graphes import *

def f_pred_boucle(boucle):
    """ Renvoie les prédécesseurs de chaque variable de la boucle souhaitée sous forme de dictionnaire
    boucle : tableau des variables qui forment la boucle que l'on veut rendre fonctionnelle """
    pred_boucle_dic = ({})
    for i in range(len(boucle)):
        if i == 0:
            pred_boucle_dic[boucle[i]] = boucle[-1]
        else:
            pred_boucle_dic[boucle[i]] = boucle[i - 1]
    return pred_boucle_dic

def f_succ_boucle(boucle):
    """ Renvoie les successeurs de chaque variable de la boucle souhaitée sous forme de dictionnaire
    boucle : tableau des variables qui forment la boucle que l'on veut rendre fonctionnelle """
    succ_boucle_dic = ({})
    for i in range(len(boucle)):
        if i + 1 >= len(boucle):
            succ_boucle_dic[boucle[i]] = boucle[0]
        else:
            succ_boucle_dic[boucle[i]] = boucle[i + 1]
    return succ_boucle_dic

def fichier_smb(nom_graph, graph_seuil, j, nom_var, d_plus, d_moins, boucle, pred_boucle, succ_boucle):
    """ Créer un fichier au format .smb avec les 4 blocs VAR, REG, PARA et FAIRCTL
    nom_graph   : nom du graphe que l'on veut générer en fichier smb
    graph_seuil : graphe d'influence avec seuil
    j           : nom de la version du graphe
    nom_var     : nom des variables du graphe
    d_plus      : degré sortant des variables du graphe
    d_moins     : degré entrant des variables du graphe
    boucle      : tableau des variables qui forment la boucle que l'on veut rendre fonctionnelle
    pred_boucle : les prédécesseurs des variables de la boucle
    succ_boucle : les successeurs des variables de la boucle """

    # créer un fichier .smb dans le dossier smb du type : graphe_1_v1
    f = open("smb/%s_v%s.smb" % (nom_graph, j), 'w')

    # bloc VAR
    f.write('VAR \n\n')

    for v in range(len(nom_var)):
        if d_plus[v] != 0:
            # ex : A1 = 0..1 ;
            f.write('%s = 0..%s ;\n\n' % (nom_var[v], d_plus[v]))

    # bloc REG
    f.write('\nREG\n\n')

    ind_reg = np.where(graph_seuil != 0)
    reg = graph_seuil[ind_reg]
    dic_reg = ({})
    for k in range(len(reg)):
        pred = nom_var[ind_reg[0][k]]
        succ = nom_var[ind_reg[1][k]]
        # créer dictionnaire du nom des régulations ainsi que leur seuil associé
        dic_reg['%s_To_%s' % (pred, succ)] = reg[k]
        # ex : A1_To_B [!(A1>=1)] => B ;
        f.write('%s_To_%s [!(%s>=%s)] => %s ;\n\n' % (pred, succ, pred, abs(reg[k]), succ))

    # bloc PARA
    f.write('\nPARA\n\n')

    # boucle for sur la boucle souhaitée
    for v in range(len(boucle)):
        # récupère les régulations de la variable v
        reg_var = list(filter(lambda x: 'To_%s' % nom_var[v] in x, dic_reg.keys()))

        # récupère le seuil de la variable v sur son successeur
        sv = abs(dic_reg[
                     list(filter(lambda x: '%s_To_%s' % (nom_var[v], succ_boucle[nom_var[v]]) in x, dic_reg.keys()))[
                         0]])

        # si d-(v)=1 et que x est ressource de v, alors K_v=0..sv-1 et K_v,x=sv..d+(v)
        if d_moins[v] == 1:
            # ex : K_A1 = 0..0 ;
            f.write('K_%s = 0..%s ;\n\n' % (nom_var[v], sv - 1))
            # ex : K_A1:C_To_A1 = 1..2 ;
            f.write('K_%s:%s = %s..%s ;\n\n' % (nom_var[v], reg_var[0], sv, d_plus[v]))

        # si d-(v)=2, d+(v)=2 alors on a K_v et K_v,y < sv,  K_v,x et K_v,x:y >= sv (avec x dans le répressilateur et y une autre ressource de v)
        if (d_moins[v] == 2) & (d_plus[v] == 2):
            defs.PARA_deg_2(f, v, reg_var, nom_var, pred_boucle, sv)

    # bloc FAIRCTL
    f.write('\nFAIRCTL\n\n')
    f.write(
        'B_oscille_02 = \nAF(B=0&AF(C>=1&AF(A1=0&AF(B=2&AF(C=0&AF(A1>=1&AF(B=0)))))))\n|\nAF(B=0&AF(C=2&AF(A1=0&AF(B=2&AF(C<=1&AF(A1>=1&AF(B=0)))))))\n|\nAF(B=0&AF(C>=1&AF(A1<=1&AF(B=2&AF(C=0&AF(A1=2&AF(B=0)))))))\n|\nAF(B=0&AF(C=2&AF(A1<=1&AF(B=2&AF(C<=1&AF(A1=2&AF(B=0))))))) ;\n\n')

    # END
    f.write('\nEND')

    # fermeture du fichier
    f.close()


# boucle sur tous les graphes d'influences non étiquetés
for i, nom_graphes in enumerate(list_graphes):
    graph = list_mat_graphes[i]
    list_graphes_seuils = defs.list_graphes_combi(graph)
    n = len(graph)

    nom_var = np.array(['A1', 'B', 'C', 'D'])
    d_plus = defs.deg_sortant(graph)
    d_moins = defs.deg_entrant(graph)

    # successeurs et prédécesseurs dans la boucle souhaitéé, ici : ABC
    boucle = nom_var[0:-1]
    pred_boucle = f_pred_boucle(boucle)
    succ_boucle = f_succ_boucle(boucle)

    nb_para = 0
    # boucle sur les graphes étiquetés (list_graphes_seuils)/n car pour séparer les graphes de list_graphes_seuils, il faut diviser par la taille d'un graphe)
    for j in range(1, int(len(list_graphes_seuils) / n)):
        # récupère un graphe d'influence dans la liste (j commence à 1 car l'indice 0 correspond à la matrice de base ou toutes les influences sont à 1, lorsque les seuils ne sont pas encore considérés)
        graph_seuil = list_graphes_seuils[j * n:(j + 1) * n]

        # création fichier smb
        fichier_smb(nom_graphes, graph_seuil, j, nom_var, d_plus, d_moins, boucle, pred_boucle, succ_boucle)

        # dynamique total des graphes d'influences
        nb_para += np.prod((d_plus + 1) ** (2 ** d_moins))

print('La dynamique totale est de %s paramétrisations.' % nb_para)

